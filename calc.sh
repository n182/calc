#!/bin/bash
while IFS= read -r arithmetic_expr
do
        res=$(( $arithmetic_expr ))
        echo "$res"
done < $1
